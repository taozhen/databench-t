/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;

import java.math.BigDecimal;

public class Branch {

    private String branch_id; //网点号
    private Integer branch_seq; //流水号配置
    private String branch_dacno; //工资借方账号
    private String branch_add; //网点地址
    private String branch_tel; //网点电话
    private String branch_fld1; 
    private String branch_fld2;
    private BigDecimal  branch_fld3; 
    private BigDecimal  branch_fld4;
    public String getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(String branch_id) {
        this.branch_id = branch_id;
    }

    public Integer getBranch_seq() {
        return branch_seq;
    }

    public void setBranch_seq(Integer branch_seq) {
        this.branch_seq = branch_seq;
    }

    public String getBranch_dacno() {
        return branch_dacno;
    }

    public void setBranch_dacno(String branch_dacno) {
        this.branch_dacno = branch_dacno;
    }

    public String getBranch_add() {
        return branch_add;
    }

    public void setBranch_add(String branch_add) {
        this.branch_add = branch_add;
    }

    public String getBranch_tel() {
        return branch_tel;
    }

    public void setBranch_tel(String branch_tel) {
        this.branch_tel = branch_tel;
    }

	public String getBranch_fld1() {
		return branch_fld1;
	}

	public void setBranch_fld1(String branch_fld1) {
		this.branch_fld1 = branch_fld1;
	}

	public String getBranch_fld2() {
		return branch_fld2;
	}

	public void setBranch_fld2(String branch_fld2) {
		this.branch_fld2 = branch_fld2;
	}

	public BigDecimal getBranch_fld3() {
		return branch_fld3;
	}

	public void setBranch_fld3(BigDecimal branch_fld3) {
		this.branch_fld3 = branch_fld3;
	}

	public BigDecimal getBranch_fld4() {
		return branch_fld4;
	}

	public void setBranch_fld4(BigDecimal branch_fld4) {
		this.branch_fld4 = branch_fld4;
	}
    
    
    
}
