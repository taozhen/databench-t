/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;


public class Trancfg {

    private String trancfg_testid; //评测编号
    private String trancfg_id; //交易代码
    private String trancfg_name; //交易名称
    private Integer trancfg_procnum; //实例数
    private Integer trancfg_runnum; //运行次数/实例
    private Integer trancfg_trannum; //事务粒度
    private String trancfg_brkpot; //断点启动
    private String trancfg_brktime; //断点停留时长

    public String getTrancfg_testid() {
        return trancfg_testid;
    }

    public void setTrancfg_testid(String trancfg_testid) {
        this.trancfg_testid = trancfg_testid;
    }

    public String getTrancfg_id() {
        return trancfg_id;
    }

    public void setTrancfg_id(String trancfg_id) {
        this.trancfg_id = trancfg_id;
    }

    public String getTrancfg_name() {
        return trancfg_name;
    }

    public void setTrancfg_name(String trancfg_name) {
        this.trancfg_name = trancfg_name;
    }

    public Integer getTrancfg_procnum() {
        return trancfg_procnum;
    }

    public void setTrancfg_procnum(Integer trancfg_procnum) {
        this.trancfg_procnum = trancfg_procnum;
    }

    public Integer getTrancfg_runnum() {
        return trancfg_runnum;
    }

    public void setTrancfg_runnum(Integer trancfg_runnum) {
        this.trancfg_runnum = trancfg_runnum;
    }

    public Integer getTrancfg_trannum() {
        return trancfg_trannum;
    }

    public void setTrancfg_trannum(Integer trancfg_trannum) {
        this.trancfg_trannum = trancfg_trannum;
    }

    public String getTrancfg_brkpot() {
        return trancfg_brkpot;
    }

    public void setTrancfg_brkpot(String trancfg_brkpot) {
        this.trancfg_brkpot = trancfg_brkpot;
    }

    public String getTrancfg_brktime() {
        return trancfg_brktime;
    }

    public void setTrancfg_brktime(String trancfg_brktime) {
        this.trancfg_brktime = trancfg_brktime;
    }
}
