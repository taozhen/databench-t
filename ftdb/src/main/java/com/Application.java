/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com;

import java.util.UUID;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.common.context.SpringContextUtils;
import com.common.schedule.HeartbeatSchedule;
import com.service.impl.DispatchServiceImpl;
import com.service.impl.InitialDataServiceImpl;
import com.service.impl.MasterNodeServiceImpl;

@SpringBootApplication
@EnableTransactionManagement
public class Application {

    public static void main(String[] args) {
    	SpringApplication springApplication = new SpringApplication(Application.class);
		springApplication.setBannerMode(Banner.Mode.OFF);
		springApplication.run(args);
        System.out.println(" 应用启动配置加载加载成功！" );
        if(args!=null&&args.length>0) {
        	if("init".equals(args[0])) {
        		System.out.println(" 数据初始化开始 ...");
        		//数据初始化
        		InitialDataServiceImpl initialDataServiceImpl = SpringContextUtils.getBean(InitialDataServiceImpl.class);
        		initialDataServiceImpl.setDatacfg_id(args[1]);
        		initialDataServiceImpl.setProcess_id(UUID.randomUUID().toString().replace("-", ""));
        		initialDataServiceImpl.run();
                System.exit(0);
        	}else if("test".equals(args[0])) {
        		System.out.println(" 测试执行开始");
        		DispatchServiceImpl dispatchServiceImpl = SpringContextUtils.getBean(DispatchServiceImpl.class);
        		dispatchServiceImpl.setDatacfg_id(args[1]);
        		dispatchServiceImpl.setProcess_id(UUID.randomUUID().toString().replace("-", ""));
        		dispatchServiceImpl.setTrancfg_testid(args[2]);
        		dispatchServiceImpl.setIsolationLevel(args[3]);
        		dispatchServiceImpl.run();
                System.exit(0);
        	}else if("master".equals(args[0])) {
        		HeartbeatSchedule.isMaster = true;
        	}else if("masterinit".equals(args[0])) {
        		System.out.println(" 数据初始化开始 ...");
        		//数据初始化
        		InitialDataServiceImpl initialDataServiceImpl = SpringContextUtils.getBean(InitialDataServiceImpl.class);
        		initialDataServiceImpl.setDatacfg_id(args[1]);
        		initialDataServiceImpl.setProcess_id(UUID.randomUUID().toString().replace("-", ""));
        		initialDataServiceImpl.run();
                System.exit(0);
        	}else if("mastertest".equals(args[0])) {
        		System.out.println(" 测试执行开始");
        		//数据初始化
            	MasterNodeServiceImpl masterNodeServiceImpl = SpringContextUtils.getBean(MasterNodeServiceImpl.class);
            	masterNodeServiceImpl.setDatacfg_id(args[1]);
            	masterNodeServiceImpl.setTrancfg_testid(args[2]);
            	masterNodeServiceImpl.setProcess_id(UUID.randomUUID().toString().replace("-", ""));
            	masterNodeServiceImpl.setIsolationLevel(args[3]);
            	masterNodeServiceImpl.run();
            	System.exit(0);
        	}
        }
    }
}
