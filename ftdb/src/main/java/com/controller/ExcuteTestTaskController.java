/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.common.util.CommUtil;
import com.common.util.DependenceUtil;
import com.service.DispatchService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class ExcuteTestTaskController {
	private final Logger logger = LoggerFactory.getLogger(ExcuteTestTaskController.class);
	
	@Autowired
	private DispatchService dispatchService;
	
	@RequestMapping("/testTask")
	@ResponseBody
	public String dealTestTask(@Param(value = "datacfg_id")String datacfg_id,
		@Param(value = "trancfg_testid")String trancfg_testid,@Param(value = "isolationLevel")String isolationLevel,
		HttpServletResponse response){
		//response.setHeader("Access-Control-Allow-Origin", "*");
		new Thread((new Runnable() {
			public void run() {
				try {
					Thread.sleep(500);
					dispatchService.testTask(datacfg_id, trancfg_testid, isolationLevel);
				} catch (InterruptedException e) {
					logger.info("ExcuteTestTaskController dealTestTask occur error:{}", e.getMessage());
				}
			}
		})).start();
		return DependenceUtil.responseResult(0, "正在执行业务操作，请稍后......", CommUtil.getRandomUUID());
	}
	

}
